"""
ASGI config for attribute_informativeness project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'attribute_informativeness.settings')
if os.environ.get('DJANGO_ENV') == 'production':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'attribute_informativeness.production')
else:
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'attribute_informativeness.settings')

application = get_asgi_application()
