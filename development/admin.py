from django.contrib import admin
from .models import testImages, Stimulus, Session, Subject, Trial

# Register your models here.
admin.site.register(testImages)
admin.site.register(Stimulus)
admin.site.register(Session)
admin.site.register(Subject)
admin.site.register(Trial)