from django.shortcuts import render, HttpResponseRedirect, reverse, HttpResponse, get_object_or_404
from django.contrib import messages
from django.core.files import File
from django import forms
from .models import Stimulus, Session, Subject, Trial
from datetime import datetime
import numpy as np
from glob import glob
import os
from secrets import token_urlsafe

#Random functions (Move to separate file later)
def stimulusCombinations(stimuli):
    stimulus_combinations = [[i, j, k] for i in stimuli['colors']
           for j in stimuli['shapes']
           for k in stimuli['textures']]
    return stimulus_combinations

all_colors = ['blue', 'magenta', 'orange', 'purple', 'yellow']
all_shapes = ['circle', 'hexagon', 'square', 'circle', 'star', 'x']
all_textures = ['capsules','checker','diagonal','dots', 'solid']

reward_rules = {
    'rule_1':
    {   'attributes': { 'color': ['orange'],
                        'shape': ['circle', 'hexagon', 'square', 'circle', 'x'],
                        'texture': all_textures},
        'reward_probabilities': {   'v': 0.90,
                                    'b': 0.00,
                                    'n': 0.00}
    },
    'rule_2':
    {
        'attributes': { 'color': ['blue', 'magenta', 'purple', 'yellow'],
                        'shape': ['star'],
                        'texture': all_textures},
        'reward_probabilities': {   'v': 0.00,
                                    'b': 0.90,
                                    'n': 0.00}
    },
    'rule_3':
    {
        'attributes': { 'color': ['orange'],
                        'shape': ['star'],
                        'texture': all_textures},
        'reward_probabilities': {   'v': 0.90,
                                    'b': 0.90,
                                    'n': 0.00}
    },
    'default':
    {
        'attributes': { 'color': 'default',
                        'shape': 'default',
                        'texture': 'default'},
        'reward_probabilities': {   'v': 0.00,
                                    'b': 0.00,
                                    'n': 0.90}
    }
}

def buildStimulusDB(reward_rules,
        file_dir='/Users/wpettine/Dropbox/_Murray/Code/state_inference_RL/online_task/stimuli/individual_png/'):
    #Loop through each image file
    f_names = glob(file_dir + '*' + '.png')
    for f in range(len(f_names)):
        shape, color, texture = os.path.basename(f_names[f]).split('.')[0].split('_')
        #Check if it exists
        if Stimulus.objects.filter(shape=shape,color=color,texture=texture).exists():
            stimulus = Stimulus.objects.filter(shape=shape, color=color, texture=texture)[0]
        else: # Create new entry
            stimulus = Stimulus(shape=shape,color=color,texture=texture)
        stimulus.image.save(os.path.basename(f_names[f]), File(open(f_names[f], 'rb')))
        #Check rules and make sure there's no overlapping membership
        rules = reward_rules.keys()
        probs_set = False
        for rule in rules:
            if (shape in reward_rules[rule]['attributes']['shape']) and \
               (color in reward_rules[rule]['attributes']['color']) and \
               (texture in reward_rules[rule]['attributes']['texture']):
                if probs_set:
                    raise ValueError("Two rules have overlapping membership")
                else:
                    stimulus.reward_probabilities = reward_rules[rule]['reward_probabilities']
                    probs_set = True
        #If they don't match a rule, use the generic settings.
        if not probs_set:
            try:
                stimulus.reward_probabilities = reward_rules['default']['reward_probabilities']
            except:
                raise ValueError("Generic reward probabilities needed")
        # Create the new entry
        stimulus.save()


def getTrialStimulus(request,N_TRIALS_BLOCK_0):
    trial_n = request.session['trial_number']
    print(trial_n)
    if request.session['current_block'] == 0:
        stimulus_key = STIMULUS_COMBINATIONS_BLOCK_0[request.session['stimulus_order_block_0'][trial_n]]
    elif request.session['current_block'] == 1:
        stimulus_key = STIMULUS_COMBINATIONS_BLOCK_1[request.session['stimulus_order_block_1'][trial_n-N_TRIALS_BLOCK_0-1]]
    else:
        current_block = request.session['current_block']
        raise ValueError(f'Current block: {current_block}')
    stimulus = Stimulus.objects.filter(color=stimulus_key[0],shape=stimulus_key[1],texture=stimulus_key[2])[0]
    return stimulus


def calcSessionPerformance(session):
    chose_largest = np.zeros(session.trials.count())
    block = chose_largest.copy()
    for t in range(session.trials.count()):
        #Determine if they chose the largest option
        response = session.trials.all()[t].response
        resp_prob = session.trials.all()[t].reward_probs_record[response]
        chose_largest[t] = int(resp_prob == max(session.trials.all()[t].reward_probs_record.values()))
        #Determine what block the trial was in
        block[t] = BLOCK_NAMES.index(session.trials.all()[t].block)
    return chose_largest, block

#Global variables
INSTRUCTIONS = "Here's the deal. You're a space pirate roving the galaxy, trying to get rich. On this quest for fame \
            and fortune, you come across a store of alien artifacts. They hold incredible power, if you can figure out \
            how to activate them. To activate them, you might shake them (press 'v'), slap them (press 'b') or kick \
            them (press 'n'). You can only figure out which one by trial and error. Sometimes it'll work, sometimes \
            it won't. Also, there's a chance that the activation method will change over time. Or maybe it will stay \
            the same. These are alien artifacts after all."
EXIT_TEXT = "Good work Space Pirate! You accumulated %d units of energy from the artifacts. To convert this to Earth  \
            currency, return home and enter the token: %s"
SUBJECT_SOURCES = [('development', 'Warren'), ('amazon', "Amazon Mechanical Turk"), ("pavlovia", "Pavlovia"),
                   ("prolific", "Prolific")]
BLOCK_NAMES = ["Conditioning", "Generalization"]
STIMULI_BLOCK_0 = {
    'colors': ['orange', 'magenta', 'yellow'],
    'shapes': ['circle', 'square', 'star'],
    'textures': ['dots']
}
STIMULI_BLOCK_1 = {
    'colors': ['orange', 'magenta', 'yellow'],
    'shapes': ['circle', 'square', 'star'],
    'textures': ['dots', 'solid', 'capsules']
}
N_TRIALS_BLOCK_0 = 5
N_TRIALS_BLOCK_1 = 2
PAYMENT_TOKEN_LENGTH = 16
STIMULUS_COMBINATIONS_BLOCK_0 = stimulusCombinations(STIMULI_BLOCK_0)
STIMULUS_COMBINATIONS_BLOCK_1 = stimulusCombinations(STIMULI_BLOCK_1)

# Forms used to pass data from pages
class RegistrationForm(forms.Form):
    user_ID = forms.CharField(label="Your ID Number")
    subject_source = forms.CharField(label="Who Sent You", widget=forms.Select(choices=SUBJECT_SOURCES))
    start_time = forms.DateTimeField(label='start_time', widget=forms.HiddenInput())


class TaskForm(forms.Form):
    key_pressed = forms.CharField(label='key_pressed', max_length=1, widget=forms.HiddenInput())
    start_time = forms.DateTimeField(label='start_time', widget=forms.HiddenInput())


# Views
def index(request):
    # return HttpResponseRedirect("development/welcome.html")
    # return HttpResponseRedirect(reverse("feedback"))
    return render(request, "development/index.html")
    # return welcome(request)

def welcome(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            #Create variables to be entered
            user_ID = form.cleaned_data["user_ID"]
            subject_source = form.cleaned_data["subject_source"]
            start_time = form.cleaned_data["start_time"]
            end_time = datetime.now() #Will update on each refresh
            payment_token = token_urlsafe(PAYMENT_TOKEN_LENGTH)
            #Check if subject exists, if not creat them
            if Subject.objects.filter(external_ID=user_ID).exists():
                subject = Subject.objects.filter(external_ID=user_ID)[0]
            else:
                subject = Subject(external_ID=user_ID, external_source=subject_source)
                subject.save()
            #Create a new Session
            session = Session(start_time=start_time, end_time=end_time, payment_token=payment_token)
            session.save()
            subject.sessions.add(session)
            #Set variables for this visit to the site
            # request.session.set_test_cookie()
            request.session['session_ID'] = session.id
            request.session['subject_ID'] = subject.id
            request.session['trial_number'] = 0
            request.session['stimulus_order_block_0'] = np.random.choice(len(STIMULUS_COMBINATIONS_BLOCK_0),
                                                                         N_TRIALS_BLOCK_0).tolist()
            request.session['stimulus_order_block_1'] = np.random.choice(len(STIMULUS_COMBINATIONS_BLOCK_1),
                                                                         N_TRIALS_BLOCK_1).tolist()
            request.session['current_block'] = 0
            return instructions(request)
    form = RegistrationForm(initial={'start_time': datetime.now()})
    # return render(request, reverse("welcome"), {
    #     "form": form
    # })
    return render(request, "development/welcome.html", {
        "form": form
    })


def instructions(request):
    #Provide the user instructions on how to perform the task
    #First, check if they have cookies enabled.
    # if request.session.test_cookie_worked():
    #     request.session.delete_test_cookie()
    # else:
    #     request.session.set_test_cookie()
    #     messages.error(request, "Please enable cookies for this site. Otherwise we can't keep track of you during \
    #                    the session and you don't get paid.")
    # return render(request, reverse("instructions"), {
    #     'instructions': INSTRUCTIONS
    # })
    return render(request, "development/instructions.html", {
        'instructions': INSTRUCTIONS
    })


def tutorial(request):
    #Provide a quick tutorial on the task
    return render(request, "development/tutorial.html")


def task(request):
    # If posting data to the page, process it
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            key_pressed = form.cleaned_data["key_pressed"]
            stimulus = getTrialStimulus(request, N_TRIALS_BLOCK_0)
            reward = np.random.rand() < stimulus.reward_probabilities[key_pressed]
            start_time = form.cleaned_data["start_time"]
            end_time = datetime.now()
            #Add the data to the trial and the trial to the session
            session = Session.objects.filter(id=request.session['session_ID'])[0]
            trial = Trial(stimulus_id=stimulus.id, reward=reward,reward_probs_record=stimulus.reward_probabilities,
                          block=BLOCK_NAMES[request.session['current_block']], start_time=start_time, end_time=end_time,
                          response=key_pressed)
            trial.save()
            session.trials.add(trial)
            session.n_trials += 1
            session.total_reward += reward
            session.end_time = end_time
            session.save()
            #Update the session variable for the next trial
            request.session['trial_number'] += 1
            if request.session['trial_number'] == N_TRIALS_BLOCK_0:
                request.session['current_block'] += 1
            if reward:
                return feedback(request, response='Rewarded! The power is growing...')
            else:
                return feedback(request, response='Nothing happens. Bummer')
    # Render this specific trial
    trial_n = request.session['trial_number']
    #If they've done all the trials, kick 'em out!
    if trial_n > (N_TRIALS_BLOCK_0 + N_TRIALS_BLOCK_1):
        return goodbye(request)

    #Otherwise, get the stimulus
    stimulus = getTrialStimulus(request,N_TRIALS_BLOCK_0)
    # Record the start of the trial and get them going on it!
    form = TaskForm(initial={'start_time': datetime.now()})
    # return render(request, reverse("task"), {
    #     "form": form,
    #     "valid_keys": list(stimulus.reward_probabilities.keys()),
    #     "stimulus_url": stimulus.image.url
    # })
    return render(request, "development/task.html", {
        "form": form,
        "valid_keys": list(stimulus.reward_probabilities.keys()),
        "stimulus_url": stimulus.image.url
    })


def feedback(request,response):
    #Tells 'em if they got it right or wrong
    # return render(request, reverse("feedback"), {
    #     'response': response
    # })
    return render(request, "development/feedback.html", {
        'response': response
    })

def goodbye(request):
    # Update the session variable values
    session = Session.objects.filter(id=request.session['session_ID'])[0]
    chose_largest, _ = calcSessionPerformance(session)
    session.final_performance = np.mean(chose_largest)
    session.session_completed = True
    # Send them off with some gold
    token = session.payment_token
    total_reward = session.total_reward
    exit_message = EXIT_TEXT % (total_reward, token)
    # return render(request, reverse("goodbye"), {
    #     'exit_message': exit_message
    # })
    return render(request, "development/goodbye.html", {
        'exit_message': exit_message
    })
