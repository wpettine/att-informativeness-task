from django.utils.safestring import mark_safe
from django.template import Library
import json

register = Library()


#Safely transfer variables from the app to javascript code
@register.filter(is_safe=True)
def js(obj):
    return mark_safe(json.dumps(obj))