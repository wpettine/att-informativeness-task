from django.urls import path

from . import views


urlpatterns = [
    path("", views.index, name="index"),
#     path("welcome", views.welcome, name="welcome"),
#     path("instructions", views.instructions, name="instructions"),
#     path("task", views.task, name="task"),
#     path("feedback", views.feedback, name="feedback"),
#     path("goodbye", views.goodbye, name="goodbye")
]