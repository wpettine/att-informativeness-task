from django.db import models

# Create your models here.
class testImages(models.Model):
    color = models.CharField(max_length=64)
    shape = models.CharField(max_length=64)
    texture = models.CharField(max_length=64)
    image = models.ImageField(upload_to='images/')


class Stimulus(models.Model):
    #Contains stimuli used in the task, organized according to attributes
    color = models.CharField(max_length=64)
    shape = models.CharField(max_length=64)
    texture = models.CharField(max_length=64)
    reward_probabilities = models.JSONField(default=dict)
    image = models.ImageField(upload_to='images/')


class Trial(models.Model):
    #All data pertinent for an individual trial
    stimulus = models.ForeignKey(Stimulus, on_delete=models.CASCADE)
    response = models.CharField(max_length=2)
    reward = models.BooleanField(null=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    block = models.CharField(max_length=20)
    reward_probs_record = models.JSONField(default=dict)


class Session(models.Model):
    #Summary details, such as length, number of stimuli seen, average performance, date, etc.
    n_trials = models.IntegerField(default=0)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    session_completed = models.BooleanField(default=False)
    total_reward = models.IntegerField(default=0)
    total_payment = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    payment_issued = models.BooleanField(default=False)
    final_performance = models.DecimalField(max_digits=6, decimal_places=5, null=True)
    trials = models.ManyToManyField(Trial, blank=True)
    payment_token = models.CharField(max_length=20)


class Subject(models.Model):
    #Contains user index, external identification number, where they came from
    external_ID = models.CharField(max_length=64)
    external_source = models.CharField(max_length=64)
    sessions = models.ManyToManyField(Session, blank=True)


